import {Component, NgZone} from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/functions';
import {CoreService} from "../services/core.service";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
      private coreService: CoreService,
      private zone: NgZone
      ) {}

  saveObject(form) {
    console.log(form.value)
    const sendEmail = firebase.functions().httpsCallable('sendEmail');

    sendEmail(
        form.value
    ).then(data => {
      this.zone.run(() => {
        this.coreService.presentAlert('Email отправлен');
        form.reset()
      });
    }).catch(error => {
      this.coreService.presentAlert('Ошибка отправки');
      console.log(error)
    });

  }

}
